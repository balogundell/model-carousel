export default [
    { id: 1, 
      img: '../../assets/images/mask-group-2.png',
      modelfName: 'Belinda', 
      modellName: 'Christelle',
      active: true,
      count: 'I',
      bgColor: '#0e1d33'
    },

    { id: 2,
      img: '../../assets/images/mask-group-2 copy.png',
      modelfName: 'Richard',
      modellName: 'Christelle',
      active: false,
      count: 'II',
      bgColor: '#543000'
    },

    { id: 3,
      img: '../../assets/images/mask-group-2 copy 2.png',
      modelfName: 'Belinda', 
      modellName: 'Christelle',
      active: false,
      count: 'III',
      bgColor: '#420422'
      },

    { id: 4,
      img: '../../assets/images/b-86-c-2446426667-585392150-be-37.png', 
      modelfName: 'Femi', 
      modellName: 'Christelle',
      active: false,
      count: 'IV',
      bgColor: '#230a22'
    },
]