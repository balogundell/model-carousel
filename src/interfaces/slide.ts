export class Slide {
  id: number;
  img: string;
  modelfName: string;
  modellName: string;
  active: boolean;
  count: string;
  bgColor:string
}